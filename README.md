## DAO

Data access object of Golite ecosystem.

## Installation

```go
go get "gitlab.com/golight/dao"
```

## Available functions and methods

```go
// NewDAO creates a new data access object.
func NewDAO(db *sqlx.DB, dbConf params.DB, scanner scanner.Scanner) *DAO
```

```go
// Begin starts a transaction.
func (s *DAO) Begin() (*sqlx.Tx, error)
```

```go
// Exec executes the specified query.
func (s *DAO) Exec(ctx context.Context, query string, tx *sqlx.Tx, args ...interface{}) (sql.Result, error)
```

```go
// LockTable locks a table for writing.
func (s *DAO) LockTable(ctx context.Context, tableName string, mode string, tx *sqlx.Tx) error
```

```go
// Ping verifies that the database connection is still alive.
func (s *DAO) Ping(ctx context.Context) error
```

```go
// Create creates a new field in the database and returns the index of this field.
func (s *DAO) Create(ctx context.Context, table tabler.Tabler, opts ...interface{}) (interface{}, error)
```

```go
// Upsert updates an existing row if a specified value already exists in a table, 
// and inserts a new row if the specified value doesn't already exist.
func (s *DAO) Upsert(ctx context.Context, entities []tabler.Tabler, opts ...interface{}) error
```

```go
// GetCount returns the number of rows that match a specified criterion.
func (s *DAO) GetCount(ctx context.Context, entity tabler.Tabler, condition params.Condition, opts ...interface{}) (uint64, error)
```

```go
// List combines the values of a table column from multiple rows into a single comma-separated list of values.
func (s *DAO) List(ctx context.Context, dest interface{}, table tabler.Tabler, condition params.Condition, opts ...interface{}) error
```

```go
// Update modifies the existing records in a table.
func (s *DAO) Update(ctx context.Context, entity tabler.Tabler, condition params.Condition, operation string, opts ...interface{}) error
```

```go
// getTransaction returns a transaction.
func getTransaction(opts ...interface{}) *sqlx.Tx
```

## Example of implementation

```go
// Обратите внимание, что вам нужно будет заменить "user=postgres password=secret dbname=postgres sslmode=disable" на строку подключения к вашей базе данных.
// Создайте подключение к базе данных
db, err := sqlx.Connect("postgres", "user=postgres password=secret dbname=postgres sslmode=disable")
if err != nil {
    log.Fatalln(err)
}

// Создайте экземпляр сканера
s := scanner.NewTableScanner()

// Создайте экземпляр DAO
d := dao.NewDAO(db, params.DB{Driver: "postgres"}, s)

// Используйте методы DAO
err = d.Ping(context.Background())
if err != nil {
    log.Fatalln(err)
}

fmt.Println("Ping successful")

// Здесь вы можете использовать другие методы DAO, такие как Create, Upsert и т.д.
// Например, d.Create(context.Background(), ваша_сущность)
```