package dao

import (
	"bytes"
	"context"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/golight/dao/errors"
	"gitlab.com/golight/dao/params"
	"gitlab.com/golight/dao/types"
	"gitlab.com/golight/entity/tabler"
	"gitlab.com/golight/scanner"
)

type MockTabler struct {
	table *scanner.Table `db:"postgres" db_ops:"UPDATE"`
}

func (m *MockTabler) TableName() string {
	return "test_table"
}

func (m *MockTabler) OnCreate() []string {
	return []string{"field1", "field2"}
}

func (m *MockTabler) FieldsPointers() []interface{} {
	return []interface{}{nil, nil}
}

type Result struct {
	ID       int    `db:"id"`
	Name     string `db:"name"`
	Postgres string `db:"postgres"`
}

func RunDB(t *testing.T) (sqlmock.Sqlmock, *sqlx.DB) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	sqlxDB := sqlx.NewDb(db, "sqlmock")
	return mock, sqlxDB
}

func ErrorHandling(err error, t *testing.T, mock sqlmock.Sqlmock) {
	if err != nil {
		t.Errorf("error was not expected: %s", err)
	}

	if err := mock.ExpectationsWereMet(); err != nil {
		t.Errorf("there were unfulfilled expectations: %s", err)
	}
}

func TestBegin(t *testing.T) {
	mock, sqlxDB := RunDB(t)

	defer sqlxDB.Close()
	d := NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())
	mock.ExpectBegin()
	tx, err := d.Begin()

	if tx == nil {
		t.Errorf("transaction was expected but got nil")
	}
	ErrorHandling(err, t, mock)
}

func TestExec(t *testing.T) {
	mock, sqlxDB := RunDB(t)

	defer sqlxDB.Close()
	d := NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())

	query := "UPDATE test_table SET name = $1 WHERE id = $2"
	args := []interface{}{"test", 1}

	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(args[0], args[1]).WillReturnResult(sqlmock.NewResult(1, 1))
	_, err := d.Exec(context.Background(), query, nil, args...)
	ErrorHandling(err, t, mock)

	mock.ExpectBegin()
	tx, _ := d.Begin()
	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(args[0], args[1]).WillReturnResult(sqlmock.NewResult(1, 1))
	_, err = d.Exec(context.Background(), query, tx, args...)
	ErrorHandling(err, t, mock)
}

func TestLockTable(t *testing.T) {
	mock, sqlxDB := RunDB(t)

	defer sqlxDB.Close()
	d := NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())

	tableName := "test_table"
	mode := "WRITE"
	mock.ExpectExec("LOCK TABLE " + tableName + " IN " + mode + " MODE").WillReturnResult(sqlmock.NewResult(1, 1))
	err := d.LockTable(context.Background(), tableName, mode, nil)

	ErrorHandling(err, t, mock)
}

func TestPing(t *testing.T) {
	mock, sqlxDB := RunDB(t)

	defer sqlxDB.Close()
	d := NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())
	mock.ExpectPing()
	err := d.Ping(context.Background())

	ErrorHandling(err, t, mock)
}

func TestCreate(t *testing.T) {
	mock, sqlxDB := RunDB(t)

	defer sqlxDB.Close()
	d := NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())

	table := &MockTabler{}
	query := "INSERT INTO test_table VALUES ()"

	mock.ExpectBegin()
	mock.ExpectExec(query).WillReturnResult(sqlmock.NewResult(1, 1))
	tx, _ := d.Begin()
	id, err := d.Create(context.Background(), table, "Meow", tx)

	if id != 1 {
		t.Errorf("expected id to be 1, but got %d", id)
	}
	ErrorHandling(err, t, mock)

	mock.ExpectExec(query).WillReturnResult(sqlmock.NewResult(1, 1))
	id, err = d.Create(context.Background(), table)

	if id != 1 {
		t.Errorf("expected id to be 1, but got %d", id)
	}
	ErrorHandling(err, t, mock)
}

func TestUpsert(t *testing.T) {
	mock, sqlxDB := RunDB(t)

	defer sqlxDB.Close()
	d := NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())
	entities := []tabler.Tabler{&MockTabler{}}

	query := "INSERT INTO test_table VALUES ()"

	mock.ExpectExec(query).WillReturnResult(sqlmock.NewResult(1, 1))
	err := d.Upsert(context.Background(), entities)
	ErrorHandling(err, t, mock)

	mock.ExpectBegin()
	mock.ExpectExec(query).WillReturnResult(sqlmock.NewResult(1, 1))
	tx, _ := d.Begin()
	err = d.Upsert(context.Background(), entities, tx)
	ErrorHandling(err, t, mock)

	mock.ExpectExec(query).WillReturnResult(sqlmock.NewResult(1, 1))
	err = d.Upsert(context.Background(), []tabler.Tabler{})
	if err == nil {
		t.Error("expected error, but got nil")
	}
}

func TestGetCount(t *testing.T) {
	mock, sqlxDB := RunDB(t)

	defer sqlxDB.Close()
	d := NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())

	entity := &MockTabler{}
	condition := params.Condition{}

	query := "SELECT COUNT(*) FROM test_table"
	rows := sqlmock.NewRows([]string{"COUNT(*)"}).AddRow(5)
	mock.ExpectQuery(regexp.QuoteMeta(query)).WillReturnRows(rows)

	count, err := d.GetCount(context.Background(), entity, condition)
	if count != 5 {
		t.Errorf("expected count to be 5, but got %d", count)
	}
	ErrorHandling(err, t, mock)

	mock.ExpectBegin()
	rows = sqlmock.NewRows([]string{"COUNT(*)"}).AddRow(5)
	mock.ExpectQuery(regexp.QuoteMeta(query)).WillReturnRows(rows)

	tx, _ := d.Begin()
	count, err = d.GetCount(context.Background(), entity, condition, tx)
	if count != 5 {
		t.Errorf("expected count to be 5, but got %d", count)
	}
	ErrorHandling(err, t, mock)
}

func TestList(t *testing.T) {
	mock, sqlxDB := RunDB(t)

	defer sqlxDB.Close()
	d := NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())

	table := &MockTabler{}
	condition := params.Condition{}

	query := "SELECT * FROM test_table"
	rows := sqlmock.NewRows([]string{"id", "name"}).AddRow(1, "test")
	mock.ExpectQuery(regexp.QuoteMeta(query)).WillReturnRows(rows)

	var dest []Result
	err := d.List(context.Background(), &dest, table, condition)
	if len(dest) != 1 {
		t.Errorf("expected one record, but got %d", len(dest))
	} else if dest[0].ID != 1 || dest[0].Name != "test" {
		t.Errorf("unexpected record: %+v", dest[0])
	}
	ErrorHandling(err, t, mock)

	rows = sqlmock.NewRows([]string{"id", "name"}).AddRow(1, "test")
	mock.ExpectBegin()
	mock.ExpectQuery(regexp.QuoteMeta(query)).WillReturnRows(rows)

	tx, _ := d.Begin()
	err = d.List(context.Background(), &dest, table, condition, tx)
	if len(dest) != 1 {
		t.Errorf("expected one record, but got %d", len(dest))
	} else if dest[0].ID != 1 || dest[0].Name != "test" {
		t.Errorf("unexpected record: %+v", dest[0])
	}
	ErrorHandling(err, t, mock)

	condition = params.Condition{
		Equal:       map[string]interface{}{"id": 4},
		NotEqual:    map[string]interface{}{"name": "2"},
		LessThan:    map[string]interface{}{"id": 5},
		GreaterThan: map[string]interface{}{"id": 2},
		Order: []*params.Order{{
			Field: "id",
			Asc:   false,
		}},
		LimitOffset: &params.LimitOffset{
			Offset: 2,
			Limit:  2,
		},
		ForUpdate: false,
		Upsert:    false,
	}
	query = "SELECT * FROM test_table WHERE id = $1 AND name <> $2 AND id < $3 AND id > $4 ORDER BY id DESC"
	rows = sqlmock.NewRows([]string{"id", "name"}).AddRow(4, "test4")

	mock.ExpectBegin()
	mock.ExpectQuery(regexp.QuoteMeta(query)).WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg()).WillReturnRows(rows)

	tx, _ = d.Begin()
	err = d.List(context.Background(), &dest, table, condition, tx)
	if len(dest) != 1 {
		t.Errorf("expected one record, but got %d", len(dest))
	} else if dest[0].ID != 4 || dest[0].Name != "test4" {
		t.Errorf("unexpected record: %+v", dest[0])
	}
	ErrorHandling(err, t, mock)

	entity := &MockTabler{table: &scanner.Table{}}
	entity.table.OperationFields = make(map[string][]*scanner.Field)
	entity.table.OperationFields["all"] = []*scanner.Field{{
		IDx:        0,
		Name:       "name",
		Type:       "",
		Default:    "",
		Constraint: scanner.Constraint{},
		Table:      &scanner.Table{},
		Pointer:    nil,
	}}
	d.statementBuilder.Scanner.RegisterTable(entity)
	condition = params.Condition{}
	query = "SELECT postgres FROM test_table"
	rows = sqlmock.NewRows([]string{"postgres"}).AddRow("test")

	mock.ExpectQuery(regexp.QuoteMeta(query)).WillReturnRows(rows)

	err = d.List(context.Background(), &dest, entity, condition)
	if len(dest) != 1 {
		t.Errorf("expected one record, but got %d", len(dest))
	} else if dest[0].ID != 0 || dest[0].Postgres != "test" {
		t.Errorf("unexpected record: %+v", dest[0])
	}
	ErrorHandling(err, t, mock)
}

func TestUpdate(t *testing.T) {
	mock, sqlxDB := RunDB(t)

	defer sqlxDB.Close()
	d := NewDAO(sqlxDB, params.DB{}, scanner.NewTableScanner())

	entity := &MockTabler{table: &scanner.Table{}}
	entity.table.OperationFields = make(map[string][]*scanner.Field)
	entity.table.OperationFields["UPDATE"] = []*scanner.Field{{
		IDx:        0,
		Name:       "name",
		Type:       "",
		Default:    "",
		Constraint: scanner.Constraint{},
		Table:      &scanner.Table{},
		Pointer:    nil,
	}}
	d.statementBuilder.Scanner.RegisterTable(entity)
	condition := params.Condition{}
	operation := "UPDATE"

	query := "UPDATE test_table SET postgres = $1"
	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(sqlmock.AnyArg()).WillReturnResult(sqlmock.NewResult(1, 1))

	err := d.Update(context.Background(), entity, condition, operation)
	ErrorHandling(err, t, mock)

	mock.ExpectBegin()
	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(sqlmock.AnyArg()).WillReturnResult(sqlmock.NewResult(1, 1))

	tx, _ := d.Begin()
	err = d.Update(context.Background(), entity, condition, operation, tx)
	ErrorHandling(err, t, mock)

	condition = params.Condition{
		Equal:    map[string]interface{}{"postgres": 1},
		NotEqual: map[string]interface{}{"postgres": 2},
	}
	query = "UPDATE test_table SET postgres = $1 WHERE postgres = $2 AND postgres <> $3"
	mock.ExpectExec(regexp.QuoteMeta(query)).WithArgs(sqlmock.AnyArg(), sqlmock.AnyArg(), sqlmock.AnyArg()).WillReturnResult(sqlmock.NewResult(1, 1))

	err = d.Update(context.Background(), entity, condition, operation)
	ErrorHandling(err, t, mock)
}

func Test(t *testing.T) {
	err := &errors.NotFound{}
	t.Log(err.Error())
	nullString := types.NewNullString("")
	nullString1 := types.NewNullString("meow")
	nullInt64 := types.NewNullInt64(0)
	nullInt64_1 := types.NewNullInt64(1)
	t.Log(types.NewNullUint64(1))
	nullFloat64 := types.NewNullFloat64(0.0)
	nullFloat64_1 := types.NewNullFloat64(0.5)
	nullBool := types.NewNullBool(!false)
	nullBool1 := types.NewNullBool(false)
	t.Log(types.NewNullTime(time.Now()))
	nullUUID := types.NewNullUUID("5c628165-729b-42b9-ba74-919ed5fb7615")
	nullUUID1 := types.NewNullUUID()
	types.NewDecoder()
	decoder1 := types.NewDecoder(jsoniter.Config{})
	var val interface{}
	var buf bytes.Buffer
	err1 := decoder1.Encode(&buf, &val)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}

	var b []byte
	b, err1 = nullString.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	err1 = nullString.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullString1.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	err1 = nullString1.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullBool.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	err1 = nullBool.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullBool1.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	err1 = nullBool1.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullInt64.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	err1 = nullInt64.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullInt64_1.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	err1 = nullInt64_1.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullFloat64.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	err1 = nullFloat64.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullFloat64_1.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	err1 = nullFloat64_1.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullUUID.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	var val2 []byte
	err1 = nullUUID.Scan(val2)
	if err1 == nil {
		t.Error("error was expected")
	}
	b = append(b, 110)
	err1 = nullUUID.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	b = append(b, 100)
	err1 = nullUUID.UnmarshalJSON(b)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullUUID.Value()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullUUID1.MarshalJSON()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	var val1 string
	err1 = nullUUID1.Scan(val1)
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
	_, err1 = nullUUID1.Value()
	if err1 != nil {
		t.Errorf("error was not expected: %s", err)
	}
}
