package mock

import (
	context "context"
	"testing"

	gomock "github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	params "gitlab.com/golight/dao/params"
	tabler "gitlab.com/golight/entity/tabler"
	"gitlab.com/golight/scanner"
)

type MockTabler struct {
	table *scanner.Table `db:"meow" db_type:"gaw" db_default:"kwa" db_index:"index,unique" db_ops:"operation1"`
}

func (m *MockTabler) TableName() string {
	return m.table.Name
}

func (m *MockTabler) OnCreate() []string {
	return []string{"field1", "field2"}
}

func (m *MockTabler) FieldsPointers() []interface{} {
	return []interface{}{nil, nil}
}


func TestBegin(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB := NewMockDAOFace(ctrl)

	mockDB.EXPECT().Begin().Return(nil, nil)

	_, err := mockDB.Begin()

	assert.NoError(t, err)
}

func TestCreate(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB := NewMockDAOFace(ctrl)
	mockTabler := &MockTabler{table: &scanner.Table{
		Name:        "users",
		Fields:      []*scanner.Field{},
		FieldsMap:   map[string]*scanner.Field{},
		Constraints: []scanner.Constraint{},
		OperationFields: map[string][]*scanner.Field{
			"operation1": {&scanner.Field{Name: "field1"}},
			"operation2": {&scanner.Field{Name: "field2"}},
		},
		Entity: nil,
	}}
	mockTabler.table.Entity = mockTabler

	mockDB.EXPECT().Create(gomock.Any(), mockTabler, gomock.Any()).Return(int64(1), nil)

	_, err := mockDB.Create(context.Background(), mockTabler, "1")

	assert.NoError(t, err)
}

func TestExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB := NewMockDAOFace(ctrl)

	query := "SELECT * FROM users"
	mockDB.EXPECT().Exec(gomock.Any(), query, nil, gomock.Any()).Return(nil, nil)

	_, err := mockDB.Exec(context.Background(), query, nil, "1")

	assert.NoError(t, err)
}

func TestGetCount(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB := NewMockDAOFace(ctrl)
	mockTabler := &MockTabler{table: &scanner.Table{
		Name:        "users",
		Fields:      []*scanner.Field{},
		FieldsMap:   map[string]*scanner.Field{},
		Constraints: []scanner.Constraint{},
		OperationFields: map[string][]*scanner.Field{
			"operation1": {&scanner.Field{Name: "field1"}},
			"operation2": {&scanner.Field{Name: "field2"}},
		},
		Entity: nil,
	}}
	mockTabler.table.Entity = mockTabler
	
	mockDB.EXPECT().GetCount(gomock.Any(), mockTabler, gomock.Any(), gomock.Any()).Return(uint64(1), nil)

	count, err := mockDB.GetCount(context.Background(), mockTabler, params.Condition{}, "1")

	assert.NoError(t, err)
	assert.Equal(t, uint64(1), count)
}

func TestList(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB := NewMockDAOFace(ctrl)
	mockTabler := &MockTabler{table: &scanner.Table{
		Name:        "users",
		Fields:      []*scanner.Field{},
		FieldsMap:   map[string]*scanner.Field{},
		Constraints: []scanner.Constraint{},
		OperationFields: map[string][]*scanner.Field{
			"operation1": {&scanner.Field{Name: "field1"}},
			"operation2": {&scanner.Field{Name: "field2"}},
		},
		Entity: nil,
	}}
	mockTabler.table.Entity = mockTabler

	mockDB.EXPECT().List(gomock.Any(), gomock.Any(), mockTabler, gomock.Any(), gomock.Any()).Return(nil)

	err := mockDB.List(context.Background(), &[]tabler.Tabler{}, mockTabler, params.Condition{}, "1")

	assert.NoError(t, err)
}

func TestLockTable(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB := NewMockDAOFace(ctrl)

	tableName := "users"
	mode := "WRITE"
	mockDB.EXPECT().LockTable(gomock.Any(), tableName, mode, nil).Return(nil)

	err := mockDB.LockTable(context.Background(), tableName, mode, nil)

	assert.NoError(t, err)
}

func TestPing(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB := NewMockDAOFace(ctrl)

	mockDB.EXPECT().Ping(gomock.Any()).Return(nil)

	err := mockDB.Ping(context.Background())

	assert.NoError(t, err)
}

func TestUpdate(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB := NewMockDAOFace(ctrl)
	mockTabler := &MockTabler{table: &scanner.Table{
		Name:        "users",
		Fields:      []*scanner.Field{},
		FieldsMap:   map[string]*scanner.Field{},
		Constraints: []scanner.Constraint{},
		OperationFields: map[string][]*scanner.Field{
			"operation1": {&scanner.Field{Name: "field1"}},
			"operation2": {&scanner.Field{Name: "field2"}},
		},
		Entity: nil,
	}}
	mockTabler.table.Entity = mockTabler

	mockDB.EXPECT().Update(gomock.Any(), mockTabler, gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)

	err := mockDB.Update(context.Background(), mockTabler, params.Condition{}, "operation", "1")

	assert.NoError(t, err)
}

func TestUpsert(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockDB := NewMockDAOFace(ctrl)
	mockTabler := &MockTabler{table: &scanner.Table{
		Name:        "users",
		Fields:      []*scanner.Field{},
		FieldsMap:   map[string]*scanner.Field{},
		Constraints: []scanner.Constraint{},
		OperationFields: map[string][]*scanner.Field{
			"operation1": {&scanner.Field{Name: "field1"}},
			"operation2": {&scanner.Field{Name: "field2"}},
		},
		Entity: nil,
	}}
	mockTabler.table.Entity = mockTabler

	mockDB.EXPECT().Upsert(gomock.Any(), []tabler.Tabler{mockTabler}, gomock.Any()).Return(nil)

	err := mockDB.Upsert(context.Background(), []tabler.Tabler{mockTabler}, "1")

	assert.NoError(t, err)
}
